/*
 * utils.hpp
 *
 *  Created on: Oct 15, 2023
 *      Author: Adel
 */

#ifndef INC_UTILS_HPP_
#define INC_UTILS_HPP_

template <class dataType>
union ByteForType
{
	dataType f;
	uint8_t bytes[sizeof(dataType)];
};

inline long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

inline uint16_t mapUint16(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

inline long mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

#endif /* INC_UTILS_HPP_ */
