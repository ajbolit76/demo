//
// Created by Adel on 30.10.2023.
//

#ifndef TESTPROJECTCPP_UARTWRITER_H
#define TESTPROJECTCPP_UARTWRITER_H

#include <cstdint>
#include "string_view"
#include "stm32f1xx_hal.h"

class UartWriter {
public:
    explicit UartWriter(UART_HandleTypeDef* uartTypeDef);
    void WriteSync(const uint8_t* bytes, int length);
    void WriteStrSync(std::string_view stringView);
private:
    UART_HandleTypeDef* mUartHandle;
};


#endif //TESTPROJECTCPP_UARTWRITER_H
