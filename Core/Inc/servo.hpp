/*
 * servo.hpp
 *
 *  Created on: Oct 15, 2023
 *      Author: Adel
 */

#ifndef INC_SERVO_HPP_
#define INC_SERVO_HPP_

#include "stm32f1xx_hal.h"
#include "utils.hpp"
#include <cstdint>
#include <cstring>

#define STM32_WRONG_PIN	255
#define REFRESH_INTERVAL        20000 // microseconds
#define TIMER_INTERVAL_MICRO 1

class STM32_ISR_Servo
{

  public:
    // maximum number of servos
    const static int MAX_SERVOS = 8;

    // constructor
    STM32_ISR_Servo();

    void run(TIM_HandleTypeDef* timer);

    // Bind servo to the timer and pin, return servoIndex
    int8_t setupServo(GPIO_TypeDef* gpio, const uint16_t& pin, const uint16_t& min, const uint16_t& max);

    // setPosition will set servo to position in degrees
    // by using PWM, turn HIGH 'duration' microseconds within REFRESH_INTERVAL (20000us)
    // returns true on success or -1 on wrong servoIndex
    bool setPosition(const uint8_t& servoIndex, const uint16_t& position, const uint16_t& maxAngle = 180);

    // returns last position in degrees if success, or -1 on wrong servoIndex
    float getPosition(const uint8_t& servoIndex);

    // setPulseWidth will set servo PWM Pulse Width in microseconds, correcponding to certain position in degrees
    // by using PWM, turn HIGH 'pulseWidth' microseconds within REFRESH_INTERVAL (20000us)
    // min and max for each individual servo are enforced
    // returns true on success or -1 on wrong servoIndex
    bool setPulseWidth(const uint8_t& servoIndex, uint16_t& pulseWidth);

    // returns pulseWidth in microsecs (within min/max range) if success, or 0 on wrong servoIndex
    uint16_t getPulseWidth(const uint8_t& servoIndex);

    // destroy the specified servo
    void deleteServo(const uint8_t& servoIndex);

    // returns true if the specified servo is enabled
    bool isEnabled(const uint8_t& servoIndex);

    // enables the specified servo
    bool enable(const uint8_t& servoIndex);

    // disables the specified servo
    bool disable(const uint8_t& servoIndex);

    // enables all servos
    void enableAll();

    // disables all servos
    void disableAll();

    // enables the specified servo if it's currently disabled,
    // and vice-versa
    bool toggle(const uint8_t& servoIndex);

    // returns the number of used servos
    int8_t getNumServos();

    // returns the number of available servos
    int8_t getNumAvailableServos()
    {
      if (numServos <= 0)
        return MAX_SERVOS;
      else
        return MAX_SERVOS - numServos;
    };

  private:
    void init()
    {
      for (uint8_t servoIndex = 0; servoIndex < MAX_SERVOS; servoIndex++)
      {
        memset((void*) &servo[servoIndex], 0, sizeof (servo_t));
        servo[servoIndex].count    = 0;
        servo[servoIndex].enabled  = false;
        // Intentional bad pin
        servo[servoIndex].pinReg   = NULL;
        servo[servoIndex].pin      = STM32_WRONG_PIN;
      }

      numServos   = 0;

      // Init timerCount
      timerCount  = 1;
    }

    // find the first available slot
    int findFirstFreeSlot();

    typedef struct
    {
      GPIO_TypeDef* pinReg;
      uint16_t       pin;                  // pin servo connected to
      uint16_t		count;                // In microsecs
      float         position;             // In degrees
      bool          enabled;              // true if enabled
      uint16_t      min;
      uint16_t      max;
    } servo_t;

    volatile servo_t servo[MAX_SERVOS];

    // actual number of servos in use (-1 means uninitialized)
    volatile int8_t numServos;

    // timerCount starts at 1, and counting up to (REFRESH_INTERVAL / TIMER_INTERVAL_MICRO) = (20000 / 10) = 2000
    // then reset to 1. Use this to calculate when to turn ON / OFF pulse to servo
    // For example, servo1 uses pulse width 1000us => turned ON when timerCount = 1, turned OFF when timerCount = 1000 / TIMER_INTERVAL_MICRO = 100
    volatile unsigned long timerCount;
};

extern STM32_ISR_Servo STM32_ISR_Servos;

#endif /* INC_SERVO_HPP_ */
