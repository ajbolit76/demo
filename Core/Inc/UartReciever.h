//
// Created by Adel on 29.10.2023.
//

#ifndef TESTPROJECTCPP_UARTRECIEVER_H
#define TESTPROJECTCPP_UARTRECIEVER_H

#include <cstdint>
#include "stm32f1xx_hal.h"
#include "RingBufCPP.hpp"
#include "UartWriter.h"
#include "CommandHandler.h"
#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "task.h"

template<size_t CacheSize>
class UartReciever{
public:
    UartReciever(
            UartWriter* uartWriter,
            CommandHandler* commandHandler){
        mCommandHandler = commandHandler;
        mUartWriter = uartWriter;
        mIsEscaping = false;
        mIsRunning = false;
        mCommandNeedProcessing = false;
        mTaskHandle = NULL;
    };
    void StartProcessing(){
        if (mIsRunning) {
            return;
        }

        if (xTaskCreate(
                UartReciever::TaskBootstrap,
                "HandleCompletedCommand",
                100,
                this,
                osPriorityAboveNormal,
                &this->mTaskHandle) == pdPASS) {
            mIsRunning = true;
        };
    };
    void OnIncomingByte(uint8_t byte) {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;
        bool writeResult;

        if(mCommandNeedProcessing){
            mUartWriter->WriteStrSync(Messages::BUSY);
            return;
        }

        if (!mIsEscaping && byte == Messages::ESCAPE_CHARACTER) {
            mIsEscaping = true;
            writeResult = true;
        } else if (mIsEscaping) {
            writeResult = mCommandBuffer.add(byte, false);
            mIsEscaping = false;
        } else {
            switch (byte) {
                case Messages::END_CHARACTER:
                    writeResult = true;
                    mCommandNeedProcessing = true;
                    xTaskNotifyFromISR(
                            mTaskHandle,
                            1,
                            eNoAction,
                            &xHigherPriorityTaskWoken);
                    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
                    break;
                default:
                    writeResult = mCommandBuffer.add(byte, false);
            }
        }

        if (!writeResult) {
            mUartWriter->WriteStrSync(Messages::NACK);
            if (!mCommandNeedProcessing) {
                mCommandBuffer.reset();
            }
        };
    }
private:
    static void TaskBootstrap(void* _this){
        static_cast<UartReciever<CacheSize>*>(_this)->TaskProcessCommand();
    };

    void TaskProcessCommand(){
        uint32_t ulNotifiedValue;

        for(;;){
            xTaskNotifyWait(0,
                                    0x00,
                                    &ulNotifiedValue,
                                    portMAX_DELAY);
            mCommandHandler->Handle(mCommandBuffer);
            mCommandNeedProcessing = false;
        }
    };

    UartWriter* mUartWriter;
    TaskHandle_t mTaskHandle;
    CommandHandler* mCommandHandler;

    bool mIsRunning;
    volatile bool mIsEscaping;
    volatile bool mCommandNeedProcessing;
    RingBufCPP<uint8_t, CacheSize> mCommandBuffer;
};


#endif //TESTPROJECTCPP_UARTRECIEVER_H