/*
 * InverseKinematics.h
 *
 *  Created on: Oct 16, 2023
 *      Author: Adel
 */

#ifndef SRC_INVERSEKINEMATICS_H_
#define SRC_INVERSEKINEMATICS_H_

#include <cstdint>
#include <array>
#include "RobotParameters.h"

#define VLINK_EFFECTOR_CENTER 4

typedef struct {
	float x,y,z;
} Point;

class InverseKinematics {
public:
	InverseKinematics(std::array<float, LINK_COUNT>* linkLengths);
	std::array<float, JOINT_COUNT> Calculate(const Point& point);
private:
    void ApplyCalibration(std::array<float, JOINT_COUNT> &angles);
    std::array<float, LINK_COUNT> *links_lengths;
};

#endif /* SRC_INVERSEKINEMATICS_H_ */
