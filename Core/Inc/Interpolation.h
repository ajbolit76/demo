//
// Created by Adel on 31.10.2023.
//

#ifndef TESTPROJECTCPP_INTERPOLATION_H
#define TESTPROJECTCPP_INTERPOLATION_H

#include <array>
#include <cstdint>
#include <limits>
#include <cstddef>
#include "FixedPoint/fixed.h"
#include "FixedPoint/math.h"

template<size_t Size>
class MultidimensionalInterpolator {
public:
    bool Start(
            std::array<float, Size> &start,
            std::array<float, Size> &target,
            uint32_t duration) {
        if (duration < 1) {
            return false;
        }

        mDuration = duration;
        mStartValues = start;
        mStartTime = xTaskGetTickCount();
        mTarget = target;
        for (size_t i = 0; i < Size; i++) {
            mTargetDelta[i] = target[i] - start[i];
        }
        mIsRunning = true;

        return true;
    }

    std::array<float, Size> GetCurrent(uint32_t currentTime) {
        if (!mIsRunning) {
            return mTarget;
        }
        auto dt = GetDelta(currentTime);
        if (dt > mDuration) {
            mIsRunning = false;
        }
        auto t = std::min(dt / (float) mDuration, 1.0f);
        return GetInternal(t);
    };

    bool IsRunning() {
        return mIsRunning;
    };
protected:
    virtual std::array<float, Size> GetInternal(float x) = 0;

    uint32_t GetDelta(uint32_t currentTime) {
        if (currentTime < mStartTime) {
            return std::numeric_limits<uint32_t>::max() - mStartTime + currentTime;
        } else {
            return currentTime - mStartTime;
        }
    }

    bool mIsRunning = false;
    uint32_t mStartTime = 0;
    uint32_t mDuration = 0;
    std::array<float, Size> mTargetDelta;
    std::array<float, Size> mTarget;
    std::array<float, Size> mStartValues;
};


template<size_t Size>
class SmoothStepMultiDimInterpolator : public MultidimensionalInterpolator<Size> {
protected:
    __attribute__((optimize(0)))
    std::array<float, Size> GetInternal(float t) override {
        fpm::fixed_8_24 x{t};
//         M2
        x = x * x * x * (x * (6 * x - 15) + 10);

        // M1
//        auto tp4 = tf*tf*tf*tf;
//        auto tp5 = tp4*tf;
//        auto tp6 = tp5*tf;
//        auto tp7 = tp5*tf;
//        auto x = -20 * tp7;
//        x += 70 * tp6;
//        x -= 84 * tp5;
//        x += 35 * tp4;

//        auto s = fpm::signbit(x - half) ? fpm::fixed_8_24{-1} : fpm::fixed_8_24{1};
//        auto o = (1 + s) / 2;
//        x = o - half * s * fpm::pow(2 * (o - s * x), k);

        std::array<float, Size> cache;
        for (auto i = 0; i < Size; i++) {
            cache[i] = this->mStartValues[i] + this->mTargetDelta[i] * (float) x;
        }

        return cache;
    }

private:
    const fpm::fixed_8_24 k{8};
    const fpm::fixed_8_24 half{0.5};
};

#endif //TESTPROJECTCPP_INTERPOLATION_H
