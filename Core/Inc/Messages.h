//
// Created by Adel on 30.10.2023.
//

#ifndef TESTPROJECTCPP_MESSAGES_H
#define TESTPROJECTCPP_MESSAGES_H

#include <string_view>
#include "RobotParameters.h"
#include "RingBufCPP.hpp"
#include "UartWriter.h"
#include "MotorController.h"

namespace Messages {
    const std::string_view INVALID = "INVALID\n";
    const std::string_view READY = "READY\n";
    const std::string_view NACK = "NACK\n";
    const std::string_view BUSY = "BUSY\n";
    const std::string_view ACK = "ACK\n";
    const uint8_t ESCAPE_CHARACTER = 0xfe;
    const uint8_t END_CHARACTER = 0xff;

    enum CommandTypes{
        SetRawAngles = 0x01,
        SetKinematicsPosition = 0x02
    };

    class MessageHandler {
    public:
        virtual CommandTypes GetCommandType() = 0;
        virtual void Handle(ReadableBuffer<uint8_t>& buffer) = 0;
    };

    struct SetRawAnglesMessage{
        float angles[JOINT_COUNT];
    };

    struct SetKinematicsPosition{
        float x;
        float y;
        float z;
        uint16_t claw;
    };

    class SetRawAnglesMessageHandler : public MessageHandler {
    public:
        explicit SetRawAnglesMessageHandler(
                MotorController* motorController,
                UartWriter* uartWriter);
        void Handle(ReadableBuffer<uint8_t> &buffer) override;
        CommandTypes GetCommandType() override;
    private:
        UartWriter* mUartWriter;
        MotorController* mMotorController;
    };

    class SetKinematicsPositionHandler : public MessageHandler {
    public:
        explicit SetKinematicsPositionHandler(
                MotorController* motorController,
                UartWriter* uartWriter);
        void Handle(ReadableBuffer<uint8_t> &buffer) override;
        CommandTypes GetCommandType() override;
    private:
        UartWriter* mUartWriter;
        MotorController* mMotorController;
    };
}

#endif //TESTPROJECTCPP_MESSAGES_H
