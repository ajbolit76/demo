//
// Created by Adel on 30.10.2023.
//

#ifndef TESTPROJECTCPP_COMMANDHANDLER_H
#define TESTPROJECTCPP_COMMANDHANDLER_H

#include "Messages.h"
#include "RingBufCPP.hpp"


class CommandHandler {
public:
    virtual bool Handle(ReadableBuffer<uint8_t>& buffer) = 0;
};

template<unsigned int HandlerCount>
class StaticCommandHandler : public CommandHandler{
public:
    explicit StaticCommandHandler(Messages::MessageHandler*(&handlers)[HandlerCount]) {
        mHandlers = &handlers;
    };
    bool Handle(ReadableBuffer<uint8_t>& buffer) override {
        uint8_t type;
        bool res = true;
        buffer.pull(&type);

        for (auto handler : *mHandlers) {
            if(type == handler->GetCommandType()){
                handler->Handle(buffer);
                return true;
            }
        }

        return false;
    };
private:
    Messages::MessageHandler* (*mHandlers)[HandlerCount];
};




#endif //TESTPROJECTCPP_COMMANDHANDLER_H
