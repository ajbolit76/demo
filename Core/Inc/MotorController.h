//
// Created by Adel on 31.10.2023.
//

#ifndef TESTPROJECTCPP_MOTORCONTROLLER_H
#define TESTPROJECTCPP_MOTORCONTROLLER_H

#include "InverseKinematics.hpp"
#include "stm32f1xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"
#include "Interpolation.h"
#include "UartWriter.h"

typedef struct {
    GPIO_TypeDef *gpio;
    uint16_t pin;
    uint16_t maxAngle;
    uint16_t startAngle;
} MotorDescription;

typedef struct {
    uint16_t servoIndex;
    GPIO_TypeDef *gpio;
    uint16_t pin;
    uint16_t maxAngle;
    uint16_t currentAngle;
} MotorInternalDescription;

class MotorController {
public:
    MotorController(
            std::array<MotorDescription, NUM_SERVOS> &motorDescription,
            UartWriter* uartWriter,
            MultidimensionalInterpolator<NUM_SERVOS> *multidimensionalInterpolator,
            InverseKinematics *ik);

    void SetAngle(std::array<float,NUM_SERVOS> &angles, uint32_t duration = 0);
    void SetPoint(Point& point, uint32_t duration = 0);
    void StartProcessing();
private:
    static void TaskBootstrap(void *_this);

    [[noreturn]] void TaskProcessCommand();

    bool mIsRunning = false;
    MotorInternalDescription mMotors[NUM_SERVOS] = {};
    MultidimensionalInterpolator<NUM_SERVOS> *mInterpolator;
    UartWriter* mUartWriter;
    InverseKinematics *mIk;
    TaskHandle_t mTaskHandle = NULL;
};


#endif //TESTPROJECTCPP_MOTORCONTROLLER_H
