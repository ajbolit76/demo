//
// Created by Adel on 30.10.2023.
//

#include "UartWriter.h"

UartWriter::UartWriter(UART_HandleTypeDef *uartTypeDef) {
    mUartHandle = uartTypeDef;
}

void UartWriter::WriteSync(const uint8_t* bytes, int length) {
    HAL_UART_Transmit(mUartHandle, bytes, length, 1000);
}

void UartWriter::WriteStrSync(std::string_view stringView) {
    HAL_UART_Transmit(mUartHandle, reinterpret_cast<const uint8_t*>(stringView.data()), stringView.length(), 1000);
}
