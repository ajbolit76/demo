/*
 * servo.cpp
 *
 *  Created on: Oct 15, 2023
 *      Author: Adel
 */

#include "servo.hpp"

STM32_ISR_Servo STM32_ISR_Servos;


STM32_ISR_Servo::STM32_ISR_Servo()
{
}

volatile uint32_t CumulativeCountSinceRefresh = 0;
const uint16_t resetTicks = REFRESH_INTERVAL / TIMER_INTERVAL_MICRO;

void STM32_ISR_Servo::run(TIM_HandleTypeDef* timer)
{
  static volatile int servoIndex = -1;

  if(servoIndex < 0 )
	  CumulativeCountSinceRefresh = 0; // channel set to -1 indicated that refresh interval completed so reset the timer
  else {
    if(servoIndex < STM32_ISR_Servos.numServos && servo[servoIndex].enabled == true)
    	HAL_GPIO_WritePin(servo[servoIndex].pinReg, servo[servoIndex].pin, GPIO_PIN_RESET);
  }

  servoIndex++;

  if(servoIndex < STM32_ISR_Servos.numServos) {
	timer->Instance->ARR = servo[servoIndex].count;
	timer->Init.Period = servo[servoIndex].count;
	CumulativeCountSinceRefresh += servo[servoIndex].count;
    if(servo[servoIndex].enabled)     // check if activated
    	HAL_GPIO_WritePin(servo[servoIndex].pinReg, servo[servoIndex].pin, GPIO_PIN_SET); // its an active channel so pulse it high
  }
  else {
    // finished all channels so wait for the refresh period to expire before starting over
    if (CumulativeCountSinceRefresh + 4 < resetTicks ){
    	timer->Instance->ARR = resetTicks - CumulativeCountSinceRefresh;
    	timer->Init.Period = resetTicks - CumulativeCountSinceRefresh;
    }
    else
    	HAL_TIM_GenerateEvent(timer, TIM_EVENTSOURCE_UPDATE);
    servoIndex = -1; // this will get incremented at the end of the refresh period to start again at the first channel
  }

}

// find the first available slot
// return -1 if none found
int STM32_ISR_Servo::findFirstFreeSlot()
{
  // all slots are used
  if (numServos >= MAX_SERVOS)
    return -1;

  // return the first slot with no count (i.e. free)
  for (uint8_t servoIndex = 0; servoIndex < MAX_SERVOS; servoIndex++)
  {
    if (servo[servoIndex].enabled == false)
    {
      return servoIndex;
    }
  }

  // no free slots found
  return -1;
}

int8_t STM32_ISR_Servo::setupServo(GPIO_TypeDef* gpio, const uint16_t& pin, const uint16_t& min, const uint16_t& max)
{
  int servoIndex;

  if (gpio == NULL)
    return -1;

  if (numServos < 0)
    init();

  servoIndex = findFirstFreeSlot();

  if (servoIndex < 0)
    return -1;

  servo[servoIndex].pinReg     = gpio;
  servo[servoIndex].pin        = pin;
  servo[servoIndex].min        = min;
  servo[servoIndex].max        = max;
  servo[servoIndex].count      = (min+max) / (2 * TIMER_INTERVAL_MICRO);
  servo[servoIndex].position   = 0;
  servo[servoIndex].enabled    = true;

  numServos++;

  return servoIndex;
}

bool STM32_ISR_Servo::setPosition(const uint8_t& servoIndex, const uint16_t& position, const uint16_t& maxAngle)
{
  if (servoIndex >= MAX_SERVOS)
    return false;

  // Updates interval of existing specified servo
  if ( servo[servoIndex].enabled && (servo[servoIndex].pinReg != NULL))
  {
    servo[servoIndex].position  = position;
    servo[servoIndex].count     = mapUint16(position, 0, maxAngle, servo[servoIndex].min,
                                      servo[servoIndex].max) / TIMER_INTERVAL_MICRO;
    return true;
  }

  // false return for non-used numServo or bad pin
  return false;
}

// returns last position in degrees if success, or -1 on wrong servoIndex
float STM32_ISR_Servo::getPosition(const uint8_t& servoIndex)
{
  if (servoIndex >= MAX_SERVOS)
    return -1.0f;

  // Updates interval of existing specified servo
  if ( servo[servoIndex].enabled && (servo[servoIndex].pinReg != NULL))
  {
    return (servo[servoIndex].position);
  }

  // return 0 for non-used numServo or bad pin
  return -1.0f;
}


// setPulseWidth will set servo PWM Pulse Width in microseconds, correcponding to certain position in degrees
// by using PWM, turn HIGH 'pulseWidth' microseconds within REFRESH_INTERVAL (20000us)
// min and max for each individual servo are enforced
// returns true on success or -1 on wrong servoIndex
bool STM32_ISR_Servo::setPulseWidth(const uint8_t& servoIndex, uint16_t& pulseWidth)
{
  if (servoIndex >= MAX_SERVOS)
    return false;

  // Updates interval of existing specified servo
  if (servo[servoIndex].enabled && (servo[servoIndex].pinReg != NULL))
  {
    if (pulseWidth < servo[servoIndex].min)
      pulseWidth = servo[servoIndex].min;
    else if (pulseWidth > servo[servoIndex].max)
      pulseWidth = servo[servoIndex].max;

    servo[servoIndex].count     = pulseWidth / TIMER_INTERVAL_MICRO;
    servo[servoIndex].position  = map(pulseWidth, servo[servoIndex].min, servo[servoIndex].max, 0, 180);

    return true;
  }

  // false return for non-used numServo or bad pin
  return false;
}

// returns pulseWidth in microsecs (within min/max range) if success, or 0 on wrong servoIndex
uint16_t STM32_ISR_Servo::getPulseWidth(const uint8_t& servoIndex)
{
  if (servoIndex >= MAX_SERVOS)
    return 0;

  // Updates interval of existing specified servo
  if ( servo[servoIndex].enabled && (servo[servoIndex].pinReg != NULL))
  {
    return (servo[servoIndex].count * TIMER_INTERVAL_MICRO );
  }

  // return 0 for non-used numServo or bad pin
  return 0;
}


void STM32_ISR_Servo::deleteServo(const uint8_t& servoIndex)
{
  if ( (numServos == 0) || (servoIndex >= MAX_SERVOS) )
  {
    return;
  }

  // don't decrease the number of servos if the specified slot is already empty
  if (servo[servoIndex].enabled)
  {
    memset((void*) &servo[servoIndex], 0, sizeof (servo_t));

    servo[servoIndex].enabled   = false;
    servo[servoIndex].position  = 0;
    servo[servoIndex].count     = 0;
    // Intentional bad pin, good only from 0-16 for Digital, A0=17
    servo[servoIndex].pin       = STM32_WRONG_PIN;

    // update number of servos
    numServos--;
  }
}

bool STM32_ISR_Servo::isEnabled(const uint8_t& servoIndex)
{
  if (servoIndex >= MAX_SERVOS)
    return false;

  if (servo[servoIndex].pinReg == NULL)
  {
    // Disable if something wrong
    servo[servoIndex].pin     = STM32_WRONG_PIN;
    servo[servoIndex].enabled = false;
    return false;
  }

  return servo[servoIndex].enabled;
}

bool STM32_ISR_Servo::enable(const uint8_t& servoIndex)
{
  if (servoIndex >= MAX_SERVOS)
    return false;

  if (servo[servoIndex].pinReg == NULL)
  {
    // Disable if something wrong
    servo[servoIndex].pin     = STM32_WRONG_PIN;
    servo[servoIndex].enabled = false;
    return false;
  }

  if ( servo[servoIndex].count >= servo[servoIndex].min / TIMER_INTERVAL_MICRO )
    servo[servoIndex].enabled = true;

  return true;
}

bool STM32_ISR_Servo::disable(const uint8_t& servoIndex)
{
  if (servoIndex >= MAX_SERVOS)
    return false;

  servo[servoIndex].enabled = false;

  return true;
}

void STM32_ISR_Servo::enableAll()
{
  // Enable all servos with a enabled and count != 0 (has PWM) and good pin

  for (uint8_t servoIndex = 0; servoIndex < MAX_SERVOS; servoIndex++)
  {
    if ( (servo[servoIndex].count >= servo[servoIndex].min / TIMER_INTERVAL_MICRO ) && !servo[servoIndex].enabled
         && (servo[servoIndex].pinReg != NULL))
    {
      servo[servoIndex].enabled = true;
    }
  }
}

void STM32_ISR_Servo::disableAll()
{
  // Disable all servos
  for (uint8_t servoIndex = 0; servoIndex < MAX_SERVOS; servoIndex++)
  {
    servo[servoIndex].enabled = false;
  }
}

bool STM32_ISR_Servo::toggle(const uint8_t& servoIndex)
{
  if (servoIndex >= MAX_SERVOS)
    return false;

  servo[servoIndex].enabled = !servo[servoIndex].enabled;

  return true;
}

int8_t STM32_ISR_Servo::getNumServos()
{
  return numServos;
}
