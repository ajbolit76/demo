//
// Created by Adel on 31.10.2023.
//

#include "Messages.h"
#include "utils.hpp"

namespace Messages {
    SetRawAnglesMessageHandler::SetRawAnglesMessageHandler(
            MotorController *motorController,
            UartWriter *uartWriter) {
        mUartWriter = uartWriter;
        mMotorController = motorController;
    }

    void SetRawAnglesMessageHandler::Handle(ReadableBuffer<uint8_t> &buffer) {
        bool res = true;
        std::array<float, NUM_SERVOS> angles = {};
        for (int i = 0; i < NUM_SERVOS; i++) {
            int16_t temp = 0;
            auto *repr = (ByteForType<int16_t> *) &temp;
            res &= buffer.pull(&repr->bytes[0]);
            res &= buffer.pull(&repr->bytes[1]);
            if(temp < 0){
                angles[i] = -1;
            }else{
                angles[i] = temp / 10.0f;
            }

        }

        int16_t delay = 0;
        auto *repr = (ByteForType<int16_t> *) &delay;
        res &= buffer.pull(&repr->bytes[0]);
        res &= buffer.pull(&repr->bytes[1]);

        if (res) {
            mMotorController->SetAngle(angles, delay);
        } else {
            mUartWriter->WriteStrSync(INVALID);
        }
    }

    CommandTypes SetRawAnglesMessageHandler::GetCommandType() {
        return SetRawAngles;
    }

    SetKinematicsPositionHandler::SetKinematicsPositionHandler(
            MotorController *motorController,
            UartWriter *uartWriter) {
        mUartWriter = uartWriter;
        mMotorController = motorController;
    }

    void SetKinematicsPositionHandler::Handle(ReadableBuffer<uint8_t> &buffer) {
        bool res = true;
        ByteForType<Point> point = {};
        ByteForType<uint16_t> duration = {};

        for(unsigned char & byte : point.bytes){
            res &= buffer.pull(&byte);
        }
        res &= buffer.pull(&duration.bytes[0]);
        res &= buffer.pull(&duration.bytes[1]);

        if(res){
            mMotorController->SetPoint(point.f, duration.f);
        }else{
            mUartWriter->WriteStrSync(INVALID);
        }

    }

    CommandTypes SetKinematicsPositionHandler::GetCommandType() {
        return SetKinematicsPosition;
    }
}