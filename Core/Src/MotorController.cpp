//
// Created by Adel on 31.10.2023.
//

#include "MotorController.h"
#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "servo.hpp"
#include "Messages.h"

MotorController::MotorController(
        std::array<MotorDescription, NUM_SERVOS>& motorDescription,
        UartWriter* uartWriter,
        MultidimensionalInterpolator<5> *multidimensionalInterpolator,
        InverseKinematics *ik) {
    mInterpolator = multidimensionalInterpolator;
    mIk = ik;
    mUartWriter = uartWriter;

    for (int i = 0; i < NUM_SERVOS; i++) {
        auto current = motorDescription[i];
        mMotors[i] = {
                .gpio = current.gpio,
                .pin = current.pin,
                .maxAngle = current.maxAngle,
                .currentAngle = current.startAngle,
        };
    };
}

void MotorController::StartProcessing() {
    if (mIsRunning) {
        return;
    }

    for (auto & mMotor : mMotors) {
        mMotor.servoIndex = STM32_ISR_Servos.setupServo(
                mMotor.gpio,
                mMotor.pin,
                500,
                2500);
    }

    if (xTaskCreate(
            MotorController::TaskBootstrap,
            "UpdateServoPositions",
            100,
            this,
            osPriorityHigh,
            &this->mTaskHandle) == pdPASS) {
        mIsRunning = true;
    };
}

void MotorController::TaskBootstrap(void *_this) {
    static_cast<MotorController *>(_this)->TaskProcessCommand();
};

[[noreturn]] void MotorController::TaskProcessCommand() {
    bool wasInterpolating = false;
    for(;;){
        for(auto & mMotor : mMotors){
            STM32_ISR_Servos.setPosition(mMotor.servoIndex, mMotor.currentAngle, mMotor.maxAngle*10);
        }

        if(mInterpolator->IsRunning()){
            wasInterpolating = true;
            auto angles = mInterpolator->GetCurrent(xTaskGetTickCount());
            for(uint8_t x = 0; x < NUM_SERVOS; x++){
                mMotors[x].currentAngle = angles[x]*10;
            }
            vTaskDelay(10);
        }else{
            if(wasInterpolating){
                wasInterpolating = false;
                mUartWriter->WriteStrSync(Messages::READY);
            }
            vTaskDelay(100);
        }
    }
}

void MotorController::SetPoint(Point& point, uint32_t duration) {
    auto angles = mIk->Calculate(point);
    std::array<float, NUM_SERVOS> toPass = {};

    for (int i = 0; i < toPass.size(); i++){
        if(i < angles.size()){
            toPass[i] = angles[i];
        }else{
            toPass[i] = -1;
        }
    }

    SetAngle(toPass, duration);
}

void MotorController::SetAngle(std::array<float,NUM_SERVOS> &angles, uint32_t duration) {
    if(duration > 10){
        std::array<float, NUM_SERVOS> start = {};
        for (int i = 0; i < angles.size(); ++i) {
            start[i] = mMotors[i].currentAngle / 10.0f;
        }
        mInterpolator->Start(start, angles, duration);
    }else{
        for (int i = 0; i < angles.size(); ++i) {
            if(angles[i] > 0){
                mMotors[i].currentAngle = angles[i]*10;
            }
        }
    }
    xTaskAbortDelay(this->mTaskHandle);
}
