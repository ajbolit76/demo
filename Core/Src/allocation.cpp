/*
 * allocation.cpp
 *
 *  Created on: Oct 12, 2023
 *      Author: Adel
 */
#include <new>
#include <cstdlib>

void* operator new(std::size_t size)
{
    return malloc(size);
}

void operator delete(void* ptr)
{
    free(ptr);
}

void* operator new[](std::size_t size)
{
    return malloc(size);
}

void operator delete[](void* ptr)
{
    free(ptr);
}
