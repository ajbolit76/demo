/*
 * InverseKinematics.cpp
 *
 *  Created on: Oct 16, 2023
 *      Author: Adel
 */

#include <InverseKinematics.hpp>
#include <cmath>

#define LINK(num) (*links_lengths)[num]
constexpr float degPerRad = 180 / 3.14159265358f;

float toDegree(float radian){
	return (radian * degPerRad);
}

InverseKinematics::InverseKinematics(std::array<float, LINK_COUNT>* linkLengths){
	links_lengths = linkLengths;
}

std::array<float, JOINT_COUNT> InverseKinematics::Calculate(const Point &point) {
    std::array<float, JOINT_COUNT> dest = {};
	float r1 = (sqrtf(point.x*point.x + point.y*point.y) - LINK(VLINK_EFFECTOR_CENTER))
			/ sqrtf(point.x*point.x + point.y*point.y);

	float x1 = r1 * point.x;
	float y1 = r1 * point.y;

	float A = sqrtf(x1*x1 + y1*y1);
	float B = LINK(3) - LINK(0) + point.z;
	float C = sqrtf(A*A + B*B);

	float a1 = (x1 >= 0) ? atanf(y1/x1) : 3.14159265358f + atanf(y1/x1);

	float phi1 = acosf((LINK(1)*LINK(1) + C*C - LINK(2)*LINK(2))/(2*LINK(1)*C));
	float phi2 = atanf(B/A);

	float a2 = phi1 + phi2;

	float a3 = acosf((C*C - LINK(1)*LINK(1) - LINK(2)*LINK(2)) / (2 * LINK(1) * LINK(2)));

	float phi3 = M_PI/2 - phi2;
	float phi4 = a3 - phi1;

	float a4 = M_PI - phi3 - phi4;

    dest = {toDegree(a1), toDegree(a2), toDegree(a3), toDegree(a4)};
	ApplyCalibration(dest);
    return dest;
}

void InverseKinematics::ApplyCalibration(std::array<float, JOINT_COUNT>& angles){
	angles[1] = 295 - angles[1]*0.88f;
	angles[2] = 150 - angles[2];
	angles[3] = 216 - angles[3];
}

